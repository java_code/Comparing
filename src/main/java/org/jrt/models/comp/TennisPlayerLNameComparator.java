package org.jrt.models.comp;

import java.util.Comparator;

import org.jrt.models.TennisPlayer;

/**
 * Comparator class for comparing the last names of tennis players
 * 
 * @author jrtobac
 *
 */
public class TennisPlayerLNameComparator implements Comparator<TennisPlayer>{
	
	/**
	 * Compares tennis players based on their last names
	 */
	@Override
	public int compare(TennisPlayer tp1, TennisPlayer tp2) {
		return tp1.getlName().compareTo(tp2.getlName());
	}

}
