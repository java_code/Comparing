package org.jrt.models.comp;

import java.util.Comparator;

import org.jrt.models.TennisPlayer;

/**
 * Comparator class for comparing the first names of tennis players
 * 
 * @author jrtobac
 *
 */
public class TennisPlayerFNameComparator implements Comparator<TennisPlayer>{
	
	/**
	 * Compares tennis players based on their first names
	 */
	@Override
	public int compare(TennisPlayer tp1, TennisPlayer tp2) {
		return tp1.getfName().compareTo(tp2.getfName());
	}

}
