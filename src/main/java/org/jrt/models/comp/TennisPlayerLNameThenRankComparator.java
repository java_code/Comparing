package org.jrt.models.comp;

import java.util.Comparator;

import org.jrt.models.TennisPlayer;

public class TennisPlayerLNameThenRankComparator implements Comparator<TennisPlayer>{

	/**
	 * Compares players based on their last name
	 * If the last names match, then it compares players based on their rank
	 */
	@Override
	public int compare(TennisPlayer tp1, TennisPlayer tp2) {
		int lastNameResult = tp1.getlName().compareTo(tp2.getlName());
		
		if(lastNameResult == 0) {
			return tp1.getRank() - tp2.getRank();
		}
		return lastNameResult;
	}
	
}
