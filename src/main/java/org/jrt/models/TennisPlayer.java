package org.jrt.models;
/**
 * Class representing tennis players
 * 
 * @author jrtobac
 *
 */
public class TennisPlayer implements Comparable<TennisPlayer> {

	private String fName;
	private String lName;
	private int rank;
	
	/**
	 * Constructor
	 * 
	 * @param fName		{@link TennisPlayer#getfName()}
	 * @param lName		{@link TennisPlayer#getlName()}
	 * @param rank		{@link TennisPlayer#getRank()}
	 */
	public TennisPlayer(String fName, String lName, int rank) {
		this.fName = fName;
		this.lName = lName;
		this.rank = rank;
	}

	/**
	 * @return	the first name of the player
	 */
	public String getfName() {
		return fName;
	}

	/**
	 * @param {@link TennisPlayer#getfName()} to set
	 */
	public void setfName(String fName) {
		this.fName = fName;
	}

	/**
	 * @return	the last name of the player
	 */
	public String getlName() {
		return lName;
	}

	/**
	 * @param {@link TennisPlayer#getlName()} to set
	 */
	public void setlName(String lName) {
		this.lName = lName;
	}

	/**
	 * @return	the atp rank of the player
	 */
	public int getRank() {
		return rank;
	}

	/**
	 * @param {@link TennisPlayer#getRank()} to set
	 */
	public void setRank(int rank) {
		this.rank = rank;
	}

	/**
	 * Implements the Comparable interface in order to allow sorting of this class in collections
	 * The higher ranked player is naturally displayed before the lower ranked player
	 */
	@Override
	public int compareTo(TennisPlayer tp) {
		return this.getRank() - tp.getRank();
	}
}
