package org.jrt.comp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.jrt.models.TennisPlayer;
import org.jrt.models.comp.TennisPlayerFNameComparator;
import org.jrt.models.comp.TennisPlayerLNameComparator;
import org.jrt.models.comp.TennisPlayerLNameThenRankComparator;
/**
 * Main class to demonstrate the comparable and comparator interfaces
 * 
 * @author jrtobac
 *
 */
public class Comp {
	public static void main(String[] args) {		
		List<TennisPlayer> topPlayers = new ArrayList<TennisPlayer>();
		
		topPlayers.add(new TennisPlayer("Novak", "Djokovic", 3));
		topPlayers.add(new TennisPlayer("Roger", "Federer", 1));
		topPlayers.add(new TennisPlayer("Andy", "Murray", 4));
		topPlayers.add(new TennisPlayer("Stan", "Wawrinka", 5));
		topPlayers.add(new TennisPlayer("Rafael", "Nadal", 2));
		topPlayers.add(new TennisPlayer("Andy", "Roddick", 20));
		topPlayers.add(new TennisPlayer("Mike", "Bryan", 188));
		topPlayers.add(new TennisPlayer("Bob", "Bryan", 189));
		
		System.out.println("Before Sort:");
		for(TennisPlayer tp : topPlayers) {
			System.out.println(tp.getRank() + " " + tp.getfName() + " " + tp.getlName());
		}
		
		Collections.sort(topPlayers);
		System.out.println("After TennisPlayer Sort:");
		for(TennisPlayer tp : topPlayers) {
			System.out.println(tp.getRank() + " " + tp.getfName() + " " + tp.getlName());
		}
		
		TennisPlayerFNameComparator fnameCompare = new TennisPlayerFNameComparator();
		Collections.sort(topPlayers, fnameCompare);
		System.out.println("After TennisPlayerFNameComparator Sort:");
		for(TennisPlayer tp : topPlayers) {
			System.out.println(tp.getRank() + " " + tp.getfName() + " " + tp.getlName());
		}
		
		TennisPlayerLNameComparator lnameCompare = new TennisPlayerLNameComparator();
		Collections.sort(topPlayers, lnameCompare);
		System.out.println("After TennisPlayerLNameComparator Sort:");
		for(TennisPlayer tp : topPlayers) {
			System.out.println(tp.getRank() + " " + tp.getfName() + " " + tp.getlName());
		}			
		
		TennisPlayerLNameThenRankComparator lNameThenRankCompare = new TennisPlayerLNameThenRankComparator();
		Collections.sort(topPlayers, lNameThenRankCompare);
		System.out.println("After TennisPlayerLNameThenRankComparator Sort:");
		for(TennisPlayer tp : topPlayers) {
			System.out.println(tp.getRank() + " " + tp.getfName() + " " + tp.getlName());
		}
	}
}
