package org.jrt.models.comp;

import static org.junit.Assert.assertTrue;

import org.jrt.models.TennisPlayer;
import org.junit.jupiter.api.Test;

/**
 * Test cases for TennisPlayerLNameComparator class
 * @author jrtobac
 *
 */
class TennisPlayerLNameComparatorTest {
	
	/**
	 * Instance of the TennisPlayerLNameComparator class to be used for the tests
	 */
	TennisPlayerLNameComparator tpLNameComp =  new TennisPlayerLNameComparator();

	/**
	 * Comparing two players with the same last name should yield a result of 0
	 *
	 */
	@Test
	void testCompareEqual() {
		TennisPlayer tp1 = new TennisPlayer("Jamie", "Murray", 200);
		TennisPlayer tp2 = new TennisPlayer("Andy", "Murray", 4);
		
		int result = tpLNameComp.compare(tp1, tp2);
		assertTrue("Expected to be equal", result == 0);
	}
	
	/**
	 * Comparing two players with different last names
	 * should yield a result > 0 if the first players last name comes after the second players
	 */
	@Test
	void testCompareGreaterThan() {
		TennisPlayer tp1 = new TennisPlayer("Andy", "Murray", 4);
		TennisPlayer tp2 = new TennisPlayer("Roger", "Federer", 1);
		
		int result = tpLNameComp.compare(tp1, tp2);
		assertTrue("Expected to be greater than", result > 0);
	}
	
	/**
	 * Comparing two players with different last names
	 * should yield a result < 0 if the first players last name comes before the second players
	 */
	@Test
	void testCompareLessThan() {
		TennisPlayer tp1 = new TennisPlayer("Roger", "Federer", 1);
		TennisPlayer tp2 = new TennisPlayer("Andy", "Murray", 4);
		
		int result = tpLNameComp.compare(tp1, tp2);
		assertTrue("Expected to be less than", result < 0);
	}

}
