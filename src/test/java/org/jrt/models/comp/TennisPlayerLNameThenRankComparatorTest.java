package org.jrt.models.comp;

import static org.junit.Assert.assertTrue;

import org.jrt.models.TennisPlayer;
import org.junit.jupiter.api.Test;

class TennisPlayerLNameThenRankComparatorTest {

	/**
	 * Instance of the TennisPlayerLNameThenRankComparator class to be used for the tests
	 */
	TennisPlayerLNameThenRankComparator tpRankThenLNameComp =  new TennisPlayerLNameThenRankComparator();

	/**
	 * Comparing two players with the same last name and rank 
	 * should yield a result of 0 
	 *
	 */
	@Test
	void testCompareEqual() {
		TennisPlayer tp1 = new TennisPlayer("Andy", "Murray", 1);
		TennisPlayer tp2 = new TennisPlayer("Jamie", "Murray", 1);
		
		int result = tpRankThenLNameComp.compare(tp1, tp2);
		assertTrue("Expected to be equal", result == 0);
	}
	
	/**
	 * Comparing two players with the same last name but different ranks 
	 * should yield a result of < 0 if the first players rank comes before the second players
	 *
	 */
	@Test
	void testCompareLessThan_EqualRankDifferentLastName() {
		TennisPlayer tp1 = new TennisPlayer("Andy", "Murray", 1);
		TennisPlayer tp2 = new TennisPlayer("Jamie", "Murray", 200);
		
		int result = tpRankThenLNameComp.compare(tp1, tp2);
		assertTrue("Expected to be less than", result < 0);
	}
	
	/**
	 * Comparing two players with the same last name but different ranks 
	 * should yield a result of > 0 if the first players rank comes after the second players
	 *
	 */
	@Test
	void testCompareGreaterThan_EqualRankDifferentLastName() {
		TennisPlayer tp1 = new TennisPlayer("Jamie", "Murray", 200);
		TennisPlayer tp2 = new TennisPlayer("Andy", "Murray", 1);		
		
		int result = tpRankThenLNameComp.compare(tp1, tp2);
		assertTrue("Expected to be greater than", result > 0);
	}
	
	/**
	 * Comparing two players with different last names
	 * should yield a result > 0 if the first players last name comes after the second players
	 */
	@Test
	void testCompareGreaterThan() {
		TennisPlayer tp1 = new TennisPlayer("Andy", "Murray", 4);
		TennisPlayer tp2 = new TennisPlayer("Roger", "Federer", 1);
		
		int result = tpRankThenLNameComp.compare(tp1, tp2);
		assertTrue("Expected to be greater than", result > 0);
	}
	
	/**
	 * Comparing two players with different last names
	 * should yield a result < 0 if the first players last name comes before the second players
	 */
	@Test
	void testCompareLessThan() {
		TennisPlayer tp1 = new TennisPlayer("Roger", "Federer", 1);
		TennisPlayer tp2 = new TennisPlayer("Andy", "Murray", 4);
		
		int result = tpRankThenLNameComp.compare(tp1, tp2);
		assertTrue("Expected to be less than", result < 0);
	}

}
