package org.jrt.models.comp;

import static org.junit.Assert.assertTrue;

import org.jrt.models.TennisPlayer;
import org.junit.jupiter.api.Test;

/**
 * Test cases for TennisPlayerFNameComparator class
 * @author jrtobac
 *
 */
class TennisPlayerFNameComparatorTest {

	/**
	 * Instance of the TennisPlayerFNameComparator class to be used for the tests
	 */
	TennisPlayerFNameComparator tpFNameComp =  new TennisPlayerFNameComparator();

	/**
	 * Comparing two players with the same first name should yield a result of 0
	 *
	 */
	@Test
	void testCompareEqual() {
		TennisPlayer tp1 = new TennisPlayer("Andy", "Roddick", 20);
		TennisPlayer tp2 = new TennisPlayer("Andy", "Murray", 4);
		
		int result = tpFNameComp.compare(tp1, tp2);
		assertTrue("Expected to be equal", result == 0);
	}
	
	/**
	 * Comparing two players with different first names
	 * should yield a result > 0 if the first players first name comes after the second players
	 */
	@Test
	void testCompareGreaterThan() {
		TennisPlayer tp1 = new TennisPlayer("Roger", "Federer", 1);
		TennisPlayer tp2 = new TennisPlayer("Andy", "Murray", 4);
		
		int result = tpFNameComp.compare(tp1, tp2);
		assertTrue("Expected to be greater than", result > 0);
	}
	
	/**
	 * Comparing two players with different first names
	 * should yield a result < 0 if the first players first name comes before the second players
	 */
	@Test
	void testCompareLessThan() {
		TennisPlayer tp1 = new TennisPlayer("Andy", "Murray", 4);
		TennisPlayer tp2 = new TennisPlayer("Roger", "Federer", 1);
		
		int result = tpFNameComp.compare(tp1, tp2);
		assertTrue("Expected to be less than", result < 0);
	}

}
