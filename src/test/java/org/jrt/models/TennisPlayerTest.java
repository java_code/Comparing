package org.jrt.models;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class TennisPlayerTest {
	
	/**
	 * The constructor should set the properties correctly
	 */
	@Test
	void testConstructor() {
		String fName = "Roger";
		String lName = "Federer";
		int rank = 1;
		
		TennisPlayer rf = new TennisPlayer(fName, lName, rank);
		
		assertEquals(fName, rf.getfName());
		assertEquals(lName, rf.getlName());
		assertEquals(rank, rf.getRank());
	}
	
	/**
	 * Comparing two players with different rankings
	 * should yield a result > 0 if the first players ranking comes after the second players
	 */
	@Test
	void testCompareToGreaterThan() {
		TennisPlayer tp1 = new TennisPlayer("Rafael", "Nadal", 2);
		TennisPlayer tp2 = new TennisPlayer("Roger", "Federer", 1);
		
		int result = tp1.compareTo(tp2);
		assertTrue("Expected to be greater than", result > 0);
	}
	
	/**
	 * Comparing two players with different rankings
	 * should yield a result < 0 if the first players ranking comes before the second players
	 */
	@Test
	void testCompareToLessThan() {
		TennisPlayer tp1 = new TennisPlayer("Roger", "Federer", 1);
		TennisPlayer tp2 = new TennisPlayer("Rafael", "Nadal", 2);
		
		int result = tp1.compareTo(tp2);
		assertTrue("Expected to be greater than", result < 0);
	}
	
	/**
	 * Comparing two players with the same rankings
	 * should yield a result 0 if the first players ranking equals the second players
	 */
	@Test
	void testCompareToEqual() {
		TennisPlayer tp1 = new TennisPlayer("Roger", "Federer", 1);
		TennisPlayer tp2 = new TennisPlayer("Rod", "Laver", 1);
		
		int result = tp1.compareTo(tp2);
		assertTrue("Expected to be greater than", result == 0);
	}

}
